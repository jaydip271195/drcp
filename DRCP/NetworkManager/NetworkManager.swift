//
//  NetworkManager.swift
//  LogicWingP
//
//  Created by Jaydip's Mackbook on 28/11/20.
//  Copyright © 2020 Jaydip's Macbook. All rights reserved.
//

import Foundation

class NetworkManager {
    
    static let shared = NetworkManager()
    
    func APICall(strUrl:String,isLoaderShow:Bool, success:@escaping (_ responseObject:Any?)-> Void){
        
        if isLoaderShow{
            APPDEL.window?.rootViewController?.showLoader()
        }
        var request = URLRequest(url: URL(string: strUrl)!)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        //request.addValue("application/json", forHTTPHeaderField: "Accept")

        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            DispatchQueue.main.async {
                APPDEL.window?.rootViewController?.dissmissLoader()
            }
            print(response!)
            do {
                let json = try JSONSerialization.jsonObject(with: data!) as! Any
                success(json)
            } catch {
                print("error")
                success(nil)
            }
        })
        task.resume()
    }
}
