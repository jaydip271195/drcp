//
//  DetailVC.swift
//  DRCP
//
//  Created by Jaydip's Mackbook on 05/01/21.
//  Copyright © 2021 Jaydip's Macbook. All rights reserved.
//

import UIKit

class DetailVC: UIViewController {
    
    //MARK:- Outlet
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblContent: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblAuthor: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    //MARK:- Variable
    var objData: ArticleData?
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setLayout()
        initWithData()
        setNavigationBar()
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    //MARK:- Init Function
    func setLayout(){
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        imgView.isUserInteractionEnabled = true
        imgView.addGestureRecognizer(tapGestureRecognizer)
    }
    func initWithData(){
        self.lblTitle.text = objData?.strTitle ?? ""
        self.lblContent.text = objData?.strContent ?? ""
        self.lblDescription.text = objData?.strDescription ?? ""
        self.imgView.loadImageUsingCache(withUrl: objData?.strPicUrl ?? "")
        self.lblAuthor.text = objData?.strAuthor ?? ""
        self.lblDate.text = Utilities.convertDateFormat(from: ServerDateTimeFormater, to: "d MMM, yyyy HH:mm a", dateString: objData?.strDate ?? "")
    }
    func setNavigationBar(){
        self.title = "Details"
    }
    
    //MARK:- Action
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer){
        
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        
        let objJTSImageInfo = JTSImageInfo()
        objJTSImageInfo.image = tappedImage.image
        objJTSImageInfo.referenceRect = tappedImage.frame
        objJTSImageInfo.referenceView = tappedImage.superview
        //isImageFull = true
        
        let objJTSImageViewController = JTSImageViewController.init(imageInfo: objJTSImageInfo, mode: .image, backgroundStyle: [])
        objJTSImageViewController?.show(from: self, transition: .fromOriginalPosition)//CBCommon.getAppDelegate()?.window?.rootViewController
    }
}

