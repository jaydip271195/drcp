//
//  WebViewVC.swift
//  DRCP
//
//  Created by Jaydip's Mackbook on 05/01/21.
//  Copyright © 2021 Jaydip's Macbook. All rights reserved.
//

import UIKit
import WebKit

class WebViewVC: UIViewController {

    //MARK:- Outlet
    @IBOutlet weak var webView: WKWebView!
    
    //MARK:- Variable
    var objData: ArticleData?
    
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setLayout()
        initWithData()
        setNavigationBar()
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    //MARK:- Init Function
    func setLayout(){}
    func initWithData(){
        let originalString = objData?.strNewsUrl ?? ""
        let escapedString = originalString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        if let url = URL.init(string: escapedString) {
            let req = URLRequest.init(url: url)
            webView.load(req)
        }
    }
    func setNavigationBar(){
        self.title = "Web View"
    }
    
    //MARK:- Action
    @IBAction func btnTestTapped(_ sender: UIButton){
        
    }

}

