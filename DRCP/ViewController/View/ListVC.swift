//
//  ListVC.swift
//  DRCP
//
//  Created by Jaydip's Mackbook on 05/01/21.
//  Copyright © 2021 Jaydip's Macbook. All rights reserved.
//

import UIKit

class ListVC: UIViewController {
    
    //MARK:- Outlet
    @IBOutlet weak var tblView: UITableView!
    
    //MARK:- Variable
    let viewModel = ViewModel()
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setLayout()
        initWithData()
        setNavigationBar()
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    //MARK:- Init Function
    func setLayout(){
        tblView.estimatedRowHeight = 100
        tblView.rowHeight = UITableView.automaticDimension
    }
    func initWithData(){
        getNewsData()
    }
    func setNavigationBar(){
        self.title = "List"
    }
    
    //MARK:- Action
    @IBAction func btnTestTapped(_ sender: UIButton){
        
    }
    
    //MARK:- WS Call
    func getNewsData(){
        
        viewModel.getNewsList { (isSuccess, message) in
            if isSuccess {
                DispatchQueue.main.async {
                    self.tblView.reloadData()
                }
            }else{
                self.showAlertView(message) { (_) in }
            }
        }
        
    }
    
    @objc func linkTapped(tapGestureRecognizer: UITapGestureRecognizer){
        
        let tapedLabel = tapGestureRecognizer.view as! UILabel
        
        
        guard let cell = tapedLabel.superview?.superview?.superview as? ListTCell else {
            return // or fatalError() or whatever
        }
        let indexPath = tblView.indexPath(for: cell)
        
        
        
        let obj = viewModel.arrArticle[indexPath?.row ?? 0]
        let vc = StoryBoard.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
        vc.objData = obj
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}

//MARK: UITableview Delegate
extension ListVC: UITableViewDelegate, UITableViewDataSource,UIScrollViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.arrArticle.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblView.dequeueReusableCell(withIdentifier:"ListTCell") as! ListTCell
        let obj = viewModel.arrArticle[indexPath.row]
        cell.lblTitle.text = obj.strTitle
        cell.lblSubTitle.text = obj.strAuthor
        cell.lblDate.text = Utilities.convertDateFormat(from: ServerDateTimeFormater, to: "d MMM, yyyy HH:mm a", dateString: obj.strDate ?? "")
        cell.lblLink.text = obj.strNewsUrl
        cell.imgView.loadImageUsingCache(withUrl: obj.strPicUrl ?? "")
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(linkTapped(tapGestureRecognizer:)))
        cell.lblLink.isUserInteractionEnabled = true
        cell.lblLink.addGestureRecognizer(tapGestureRecognizer)
        
        let attributedString = NSMutableAttributedString(string: obj.strNewsUrl ?? "")
        attributedString.addAttribute(.link, value: obj.strNewsUrl ?? "", range: NSRange(location: 0, length: obj.strNewsUrl?.count ?? 0))
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = viewModel.arrArticle[indexPath.row]
        let vc = StoryBoard.instantiateViewController(withIdentifier: "DetailVC") as! DetailVC
        vc.objData = obj
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

//MARK:- Tablviewcell Class File
class ListTCell : UITableViewCell {
    
    //MARK:- Outlet
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblLink: UnderlinedLabel!
    
    // MARK:- Variables
    override func awakeFromNib() {
        
    }
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
}
