//
//  ViewModel.swift
//  DRCP
//
//  Created by Jaydip's Mackbook on 05/01/21.
//  Copyright © 2021 Jaydip's Macbook. All rights reserved.
//

import Foundation


class ViewModel : NSObject {
    
    
    var arrArticle = [ArticleData]()
    let APIKEY = "5d0fd3c8f0b34eea97886a7dad9cf5d5"
    var listURL = "https://newsapi.org/v2/top-headlines?sources=google-news&apiKey="
    
    
    //MARK:- API Call
    func getNewsList(success:@escaping (_ isSuccess:Bool,_ message:String)-> Void){
        let url = listURL + APIKEY
        NetworkManager().APICall(strUrl: url , isLoaderShow: true) { (response) in
            print(response ?? [])
            
            
            if let res = response as? [String:Any]{
                let objResponse = DataModel(res)
                
                if objResponse.status == "ok" {
                    if let arrData = res["articles"] as? [[String:Any]] {
                        for obj in arrData{
                            self.arrArticle.append(ArticleData(obj))
                        }
                    }
                    success(true,"success")
                }else{
                    success(false,"success")
                }
            }
        }
    }
}
