//
//  DataModel.swift
//  DRCP
//
//  Created by Jaydip's Mackbook on 05/01/21.
//  Copyright © 2021 Jaydip's Macbook. All rights reserved.
//

import Foundation



class DataModel: NSObject {

    var status                     : String?
    var arrArticle                 : [ArticleData]?
   
    
    init(_ dict: [String: Any]) {
        
        status                      = dict["status"] as? String ?? ""
        arrArticle                  = dict["articles"] as? [ArticleData] ?? []
    }
}



class ArticleData: NSObject {
    
    var id                          : Int?
    var strTitle                    : String?
    var strNewsUrl                  : String?
    var strPicUrl                   : String?
    var strAuthor                   : String?
    var strDate                     : String?
    var strDescription              : String?
    var strContent                  : String?
   
    
    init(_ dict: [String: Any]) {
        
        id                          = dict["id"] as? Int ?? 0
        strTitle                    = dict["title"] as? String ?? ""
        strPicUrl                   = dict["urlToImage"] as? String ?? ""
        strNewsUrl                  = dict["url"] as? String ?? ""
        strDate                     = dict["publishedAt"] as? String ?? ""
        strAuthor                   = dict["author"] as? String ?? ""
        strDescription              = dict["description"] as? String ?? ""
        strContent                  = dict["content"] as? String ?? ""
    }
}
